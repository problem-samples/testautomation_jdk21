package ch402;

import org.openqa.selenium.WebDriver;

import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.annotations.Test;

public class BrowserOptionTest {
	  @Test
	  public void SetOtherBrowserVersion() {
	    ChromeOptions chromeOptions = new ChromeOptions();
	    // Set Chrome v85 version
	    chromeOptions.setBrowserVersion("85.0");
	    WebDriver driver = new ChromeDriver(chromeOptions);
	    try {
	      driver.get("https://selenium.dev");
	    } finally {
	      driver.quit();
	    }
	  }

	  @Test
	  public void SetOtherPlatformName() {
	    ChromeOptions chromeOptions = new ChromeOptions();
		// Set Chrome under windows OS
	    chromeOptions.setPlatformName("Windows");
	    WebDriver driver = new ChromeDriver(chromeOptions);
	    try {
	      driver.get("https://selenium.dev");
	    } finally {
	      driver.quit();
	    }
	  }
}
