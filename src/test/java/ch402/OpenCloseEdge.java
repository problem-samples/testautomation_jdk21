package ch402;

import java.time.Duration;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.testng.annotations.Test;

public class OpenCloseEdge {
  
	@Test
	public void hitGoogle() {
		
		//System.setProperty("webdriver.edge.driver","drivers/windows/msedgedriver.exe");
		WebDriver driver = new EdgeDriver();
		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(5));
		String pageTitle = driver.getTitle();
		driver.get("https://www.google.com/");
		
		//wait little bit
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		System.out.println("Page Title = " + pageTitle);
		driver.quit();
		/* cmd /c taskkill /f /im chromedriver.exe  */
	}	

}
